package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.RequestDispatcher;

import com.ideas2it.zomato.servlet.filter.LogFilter;
import com.ideas2it.zomato.servlet.filter.AuthenticationFilter;
import com.ideas2it.zomato.user.controller.UserController;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.common.Constants;

/**
 * <p>
 * Servlet implementation for user. All user related operations are done 
 * through this servlet.
 * </p>
 */
public class UserServlet extends HttpServlet {

    UserController userController = new UserController();

    private static final String REGISTER = "register";
    private static final String LOGIN = "login";
    private static final String LOGOUT = "logout";
    private static final String RESET_PASSWORD = "Reset Password";
    private static final String GIVE_ADMIN_ACCESS = "give admin access";
    private static final String ADD_ADDRESS = "add address";
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        switch (input) {
            case REGISTER :
                register(request, response);
                break;
            case LOGIN :
                login(request, response, session);
                break;
            case LOGOUT :
                logout(request, response, session);
                break;
            case RESET_PASSWORD :
                resetPassword(request, response);
                break;
            case GIVE_ADMIN_ACCESS :
                giveAdminAccess(request, response, session);
                break;
            case ADD_ADDRESS :
                addAddress(request, response, session);
                break;
        }
    }
    
    /**
     * <p>
     * Registers a new user to the application, along with a address and role.
     * </p>
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void register(HttpServletRequest request, 
                          HttpServletResponse response)
                          throws ServletException, IOException {
        int roleId = Integer.parseInt(request.getParameter("roleId"));
        User user = new User(request.getParameter("firstName"),
                             request.getParameter("lastName"),
                             Long.parseLong(request.getParameter("mobile")),
                             request.getParameter("email"),
                             request.getParameter("password"),
                             request.getParameter("petName"),
                             request.getParameter("bicycleName"));
        Address address = new Address(request.getParameter("addressLineOne"),
                              request.getParameter("addressLineTwo"),
                              request.getParameter("city"),
                              request.getParameter("state"),
                              Integer.parseInt(request.getParameter("pincode")));
        boolean isSuccessful = userController.register(user, address, roleId);
        redirectToPage(isSuccessful, Constants.REGISTER_SUCCESFUL, 
                       Constants.ERROR, response);
    }
    
    /**
     * <p>
     * Logs in the user to the application.
     * User is redirected to the next page based on the user's role.
     * If the entered email does not exist in the database or if the email 
     * validation fails or if the entered password does not match the email
     * login fails. 
     * </p>
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void login(HttpServletRequest request, 
                       HttpServletResponse response, HttpSession session)
                       throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = userController.login(email, password);
        if (user != null) {
            Cookie loginCookie = new Cookie("user", user.getEmail());
            // setting cookie to expiry in 60 mins
            loginCookie.setMaxAge(60 * 60);
            response.addCookie(loginCookie);
            session.setAttribute("user", user);
            session.setAttribute("userId", user.getId());
            redirectToPage(user, response);
        }  else {
            RequestDispatcher requestDispatcher = getServletContext()
                    .getRequestDispatcher("view/LoginUser.jsp");
            requestDispatcher.include(request, response);
            response.sendRedirect("InvalidUsernamePassword.jsp");            
        }
    }
    
    /**
     * <p>
     * Logs out the user that is already logged into the application.
     * </p>
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void logout(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        User user = (User)session.getAttribute("user");
        Cookie loginCookie = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for(Cookie cookie : cookies) {
                if (cookie.getName().equals("user")) {
                    loginCookie = cookie;
                    break;
                }
            }
        }
        if(loginCookie != null) {
            loginCookie.setMaxAge(0);
            response.addCookie(loginCookie);
        }
        session.invalidate();
        boolean isSuccessful = userController.logout(user);
        redirectToPage(isSuccessful, Constants.WELCOME, 
                       "view/AccessNotAllowed.jsp", response);
    }
    
    /**
     * <p>
     * Enables user to reset old password in case the user is not able to recall
     * their old password.
     * Prompts the user to enter the registered email along with two security 
     * questions which was answered by the user at the time of registration.
     * </p>
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    private void resetPassword(HttpServletRequest request, 
                               HttpServletResponse response)
                               throws ServletException, IOException {
        String email = (request.getParameter("email"));
        String password = request.getParameter("password");
        String petName = (request.getParameter("petName"));
        String bicycleName = (request.getParameter("bicycleName"));
        boolean isSuccesful = userController.resetPassword(email, password, 
                                                        petName, bicycleName);
        redirectToPage(isSuccesful, Constants.LOGIN_USER, 
                       Constants.INVALID_EMAIL_PASSWORD, response);
    }
    
    /**
     * <p>
     * Adds a new address to the user.
     * </p>
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void addAddress(HttpServletRequest request, 
                            HttpServletResponse response, HttpSession session)
                            throws ServletException, IOException {
        Address address = new Address(request.getParameter("addressLineOne"),
                              request.getParameter("addressLineTwo"),
                              request.getParameter("city"),
                              request.getParameter("state"),
                              Integer.parseInt(request.getParameter("pincode")));
        User user = (User)session.getAttribute("user");
        userController.addAddress(user, address);
        response.sendRedirect(Constants.CUSTOMER_HOME);
    }
    
    /**
     * <p>
     * Redirects to the next jsp page based on the boolean status. 
     * </p>  
     *
     * @param isSuccessful
     *        - boolean that determines the viewpage.
     *
     * @param successPage
     *        - jsp page is boolean is true.
     *
     * @param failurePage
     *        - jsp page is boolean is false.
     */
    private void redirectToPage(boolean isSuccessful, String successPage,
                             String failurePage, HttpServletResponse response)
                             throws ServletException, IOException {
        if (isSuccessful) {
            response.sendRedirect(successPage);
        } else {
            response.sendRedirect(failurePage);
        }
    }
    
    /**
     * <p>
     * Redirects to the next jsp page based on the user role. 
     * </p>
     *
     * @param user
     *        - user that determines the viewpage.
     *
     * @param response
     *        - response object of HTTPservlet.
     */
    public void redirectToPage(User user, HttpServletResponse response)
                               throws ServletException, IOException {
        switch (user.getRole().getId()) {
            case 1 :
                response.sendRedirect(Constants.ADMIN_HOME);
                break;
            case 2 :
                response.sendRedirect(Constants.CUSTOMER_HOME);
                break;
            case 3 :
                response.sendRedirect(Constants.RESTAURANT_HOME);
                break;
        }
    }
    
    /**
     * <p>
     * Gives admin access to a user. Only a admin user can give admin access 
     * to other users.
     * </p>
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void giveAdminAccess(HttpServletRequest request, 
                       HttpServletResponse response, HttpSession session)
                       throws ServletException, IOException {
        User adminUser = (User)session.getAttribute("user");
        User user = (User)request.getAttribute("user");
        userController.giveAdminAccess(adminUser, user);
    }
}
