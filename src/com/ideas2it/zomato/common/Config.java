package com.ideas2it.zomato.common;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException; 
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Config {

    public static SessionFactory factory;
    private static final Logger logger = Logger.getLogger(Config.class);
    
    /**
     * creates the session object to establish connection with database.
     */
    static  {
        try {
            factory = new Configuration().configure("hibernate/hibernate.cfg.xml").buildSessionFactory();
            logger.debug("SESSION FACTORY CREATED");
        } catch (Throwable exception) { 
            logger.error("SESSION FACTORY EXCEPTION" + exception);
        }
    }
}
