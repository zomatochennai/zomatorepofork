package com.ideas2it.zomato.menuItem.controller;

import java.lang.NullPointerException;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.menuItem.service.MenuItemService;
import com.ideas2it.zomato.menuItem.service.impl.MenuItemServiceImpl;
import com.ideas2it.zomato.food.service.FoodService;
import com.ideas2it.zomato.food.service.impl.FoodServiceImpl;
import com.ideas2it.zomato.category.service.CategoryService;
import com.ideas2it.zomato.category.service.impl.CategoryServiceImpl;

/**
 * Controller implementation for menu item servlet.
 */
public class MenuItemController {

    MenuItemService menuItemService = new MenuItemServiceImpl();
    FoodService foodService = new FoodServiceImpl();
    CategoryService categoryService = new CategoryServiceImpl();
    private static final Logger logger = Logger.getLogger(MenuItemController.class); 
    
    /**
     * Registers a new menu item to the restaurant.
     *
     * @param menuItem
     *        - menuItem to be added
     *
     * @param restaurantId
     *        - restaurant id of the menuitem
     */
    public void addMenuItem(MenuItem menuItem, int restaurantId) {
        try {
            menuItemService.addMenuItem(menuItem, restaurantId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Updates a existing menuItem.
     *
     * @param menuItem
     *            - menuItem object about to be updated .
     */
    public void update(MenuItem menuItem) {
        try {
            menuItemService.update(menuItem);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Get the menu item categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     */
    public Set<Category> getCategories(int restaurantId) {
        Set<Category> categories = new HashSet<>();
        try {
        categories = categoryService.getCategories(restaurantId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return categories;
    }
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     */
    public Set<Category> getAllCategories() {
        Set<Category> categories = new HashSet<>();
        try {
        categories = categoryService.getAllCategories();
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return categories;
    }
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required .
     *
     * @param categoryId
     *            - category Id whose menu items are required .
     *
     * @return allmenuitems belonging to the restaurant.
     */
    public Set<MenuItem> getMenuItems(int restaurantId, int categoryId) {
        Set<MenuItem> menuItems = new HashSet<>();
        try {
        menuItems = menuItemService.getMenuItems(restaurantId, categoryId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return menuItems;
    }
    
    /**
     * Deletes a menuItem from the restaurant.
     *
     * @param id
     *            - id of menuItem to be deleted.
     */
    public void delete(int id) {
        try {
            menuItemService.delete(id);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Adds a new food to the database.
     *
     * @param food
     *            - food to be added.
     */
    public void addFood(Food food) {
        try {
            foodService.add(food);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Removes the food from the database.
     *
     * @param id
     *            - id of food.
     */
    public void removeFood(int id) {
        try {
            foodService.remove(id);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     */
    public Set<Food> getFoods() {
        Set<Food> foods = new HashSet<>();
        try {
        foods = foodService.getFoods();
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return foods;
    }
    
    /**
     * Adds a new category to the database.
     *
     * @param id
     *            - id of category.
     */
    public void addCategory(Category category) {
        try {
            categoryService.add(category);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Removes the category from the database.
     *
     * @param id
     *            - id of category.
     */
    public void removeCategory(int id) {
        try {
            categoryService.remove(id);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
}
