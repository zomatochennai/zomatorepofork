<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Classification Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>Restaurant Classification List</h1>
    <form action="http://localhost:8080/zomato/user" method="post">
      <table style="with: 5%" align="right">
        <th>
        <tr>
          <td>
            <input type="submit" value="logout" name="submit" />
          </td>
        </tr>
        </th>
      </table>
    </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/restaurant" method="get">
        <table style="with: 50%">
          <caption><h2>List of Restaurants Classification</h2></caption>
          <input type="submit" value="get classifications" name="submit" />
          <input type="hidden" value="view/ClassificationList.jsp"
                 name="targetPage" />
          <tr>
            <th>Restaurant Classification</th>
            <th>View Restaurants</th>            
          </tr>
          <tr>          
            <c:forEach var="classification" items="${classifications}">
              <td>
                <c:out value="${classification.name}" />
              </td>
              <td>
                <a href="http://localhost:8080/zomato/restaurant
                         ?id=${classification.id}&submit=get restaurants
                         &targetPage=viewRestaurantList.jsp"> 
                <input id="viewrestaurant" type="button" value="view" />
                </a> 
              </td>
            </c:forEach>
          </tr>
        </table>
      </form>
    </div>
  </body>
</html>
